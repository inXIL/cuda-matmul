```
nvprof:
Time(%)     Calls       Avg       Min       Max  Name
 18.12%         1  196.23ms  196.23ms  196.23ms  simple_kernel
 15.67%         1  169.75ms  169.75ms  169.75ms  shared_kernel
  1.30%         1  14.101ms  14.101ms  14.101ms  transposed_kernel
  1.13%         1  12.227ms  12.227ms  12.227ms  shared_transposed_kernel
```
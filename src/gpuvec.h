#ifndef GPUVEC_H
#define GPUVEC_H
#include "gpumat.h"
struct CpuVec;
struct GpuVec {
    GpuVec(size_t dim);
    GpuVec(const GpuVec& vec);
    GpuVec(const CpuVec& vec); // i don't really want to have those constructors
    ~GpuVec();
    size_t len() const;
    CpuVec clone_to_cpu() const;
    //GpuVec shrink_clone(size_t n_elem) const;
public:
    int* vals;
private:
    size_t n_elems;
};
#endif
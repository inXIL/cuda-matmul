#ifndef UTIL_H
#define UTIL_H
#include <cstdio>
#include <cstdlib>
#include <cuda_runtime.h>
#include <driver_types.h>
#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess)
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}
namespace cpu {
   inline void randomize_array(int* a, size_t n_elems) {
      for(size_t i = 0; i < n_elems; ++i) {
        a[i] = rand() % 10;
      }
   }
}
#endif
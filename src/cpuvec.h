#ifndef CPUVEC_H
#define CPUVEC_H
#include "cpumat.h"
struct GpuVec;
struct CpuVec {
    CpuVec(size_t dim);
    CpuVec(const CpuVec& vec);
    CpuVec(const GpuVec& vec); // i don't really want to have those constructors, but i'm afraid it would become a mess without them
    ~CpuVec();
    size_t len() const;
    int& operator[](size_t id);
    const int& operator[](size_t id) const;
    void randomize();
    GpuVec clone_to_gpu() const;
    const int* data() const;
    static bool cmp(const CpuVec& vec1, const CpuVec& vec2);
public:
    int* vals; // should be making it private, but then it would force me to work around it in gpu kernel code, i guess
    // or create 2 accessors one of which is const, and it would become compilation error if const accessor is used in non-const interface
private:
    size_t n_elems;
};

#endif

#ifndef GPUMAT_H
#define GPUMAT_H
#include <cstddef>
struct CpuMat;
struct GpuMat {
    GpuMat(std::size_t dim1, std::size_t dim2);
    GpuMat(std::size_t dim);
    GpuMat(const CpuMat& cpumat);
    GpuMat(const GpuMat& gpumat);
    void copy_to_cpu(CpuMat& cpumat) const;
    CpuMat clone_to_cpu() const;
    ~GpuMat();
    size_t height() const;
    size_t width() const;
    size_t len() const;
    size_t size() const;
public:
    int* vals;
private:
    std::size_t dim1, dim2;
};
#endif
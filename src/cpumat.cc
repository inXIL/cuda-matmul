#include <cstdlib>
#include <cassert>
#include <iostream>
#include <memory.h>

#include "cpumat.h"
#include "util.h"
#include "gpumat.h"

CpuMat::CpuMat(std::size_t dim1, std::size_t dim2)
: dim1(dim1), dim2(dim2), vals(new int[dim1 * dim2])
{}

CpuMat::CpuMat(std::size_t dim)
: CpuMat(dim, dim)
{}

CpuMat::CpuMat(const CpuMat& mat)
: CpuMat(mat.height(), mat.width())
{
    memcpy(vals, mat.vals, size());
}

CpuMat::~CpuMat() {
    delete[] vals;
}

// apparently it may be broken:
int* CpuMat::operator[](size_t id) { return &vals[id * dim2]; } // not extremely type-safe and performant, but it's fine
const int* CpuMat::operator[](size_t id) const { return &vals[id * dim2]; }

void CpuMat::randomize() {
    cpu::randomize_array(vals, dim1 * dim2);
}

GpuMat CpuMat::clone_to_gpu() const {
    return GpuMat(*this);
}

void CpuMat::transpose() {
    int* tmp = new int[len()];
    for(size_t i = 0; i < len(); ++i) {
        size_t id = i / dim2 + i % dim2 * dim1;
        tmp[id] = vals[i];
    }
    std::swap(tmp, vals);
    delete[] tmp;
    std::swap(dim1, dim2);
}

void CpuMat::println() const {
    for(size_t i = 0; i < dim1; ++i) {
        for(size_t j = 0; j < dim2; ++j) {
            std::cout << vals[j + i * dim2] << " ";
        }
        std::cout << "\n";
    }
    std::cout << "\n";
}

CpuMat CpuMat::mul(const CpuMat& m1, const CpuMat& m2) {
   assert(m1.dim2 == m2.dim1);
   size_t common_dim = m1.dim2;
   CpuMat mat(m1.dim1, m2.dim2);
   for(size_t i = 0; i < mat.dim1; ++i) {
      for(size_t j = 0; j < mat.dim2; ++j) {
         mat.vals[j + i * m1.dim1] = 0;
         for(size_t k = 0; k < common_dim; ++k) {
            mat.vals[j + i * common_dim] += m1.vals[k + i * common_dim] * m2.vals[j + k * common_dim];
         }
      }
   }
   return mat;
}

bool CpuMat::cmp(const CpuMat& m1, const CpuMat& m2) {
    assert(m1.dim1 == m2.dim1);
    assert(m1.dim2 == m2.dim2);
    for(size_t i = 0; i < m1.dim1; ++i) {
        for(size_t j = 0; j < m1.dim2; ++j) {
            if (m1.vals[j + i * m1.dim2] != m2.vals[j + i * m2.dim2]) {
                return false;
            }
        }
    }
    return true;
}

size_t CpuMat::height() const { return dim1; }
size_t CpuMat::width() const { return dim2; }
size_t CpuMat::len() const { return dim1 * dim2; }
size_t CpuMat::size() const { return dim1 * dim2 * sizeof(vals[0]); }
#include <cassert>
#include <cstdio>
#include <iostream>
#include "cpumat.h"
#include "gpumat.h"
#include "cpuvec.h"
#include "gpuvec.h"
#include "util.h"


const size_t BLOCK_LEN = 100;
const size_t N_BLOCKS = 250;
const size_t N = BLOCK_LEN * N_BLOCKS;
const size_t M = BLOCK_LEN * 100;

__global__ void simple_kernel(
    const int* __restrict__ mat, const int* __restrict__ vec, int* __restrict__ retvec, const size_t height, const size_t width
) {
    size_t tid = blockIdx.x * blockDim.x + threadIdx.x;
    int tmp = 0;
    #pragma unroll
    for(size_t k = 0; k < width; ++k) {
        tmp += mat[k + tid * width] * vec[k];
    }
    if (tid < height) {
        retvec[tid] = tmp;
    }
}

__global__ void transposed_kernel(
    const int* __restrict__ mat, const int* __restrict__ vec, int* __restrict__ retvec, const size_t height, const size_t width
) {
    const size_t tid = blockIdx.x * blockDim.x + threadIdx.x;
    int tmp = 0;
    #pragma unroll
    for(size_t k = 0; k < width; ++k) {
        tmp += mat[tid + k * height] * vec[k];
    }
    if (tid < height) {
        retvec[tid] = tmp;
    }
}

__global__ void shared_kernel(
    const int * __restrict__ mat, const int * __restrict__ vec, int * __restrict__ retvec, const size_t height, const size_t width
) {
    const size_t tid = threadIdx.x + blockIdx.x * blockDim.x;

    __shared__ int shared[BLOCK_LEN];

    int tmp = 0;

    #pragma unroll
    for (size_t i = 0; i < (width + BLOCK_LEN - 1) / BLOCK_LEN; ++i) {
        if (i * BLOCK_LEN + threadIdx.x <  width) {
            shared[threadIdx.x] = vec[threadIdx.x + i * BLOCK_LEN];
        }
        else {
            shared[threadIdx.x] = 0;
        }
        __syncthreads();

        #pragma unroll
        for (size_t j = 0; j < BLOCK_LEN; ++j) {
            tmp += mat[tid * width + (j + BLOCK_LEN * i)] * shared[j];
        }
        __syncthreads();
    }

    if (tid < height) {
        retvec[tid] = tmp;
    }
}

__global__ void shared_transposed_kernel(
    const int * __restrict__ mat, const int * __restrict__ vec, int * __restrict__ retvec, const size_t height, const size_t width
) {
    const size_t tid = threadIdx.x + blockIdx.x * blockDim.x;

    __shared__ int shared[BLOCK_LEN];

    int tmp = 0;

    #pragma unroll
    for (size_t i = 0; i < (width + BLOCK_LEN - 1) / BLOCK_LEN; ++i)
    {
        if (i * BLOCK_LEN + threadIdx.x <  width) {
            shared[threadIdx.x] = vec[threadIdx.x + i * BLOCK_LEN];
        }
        else {
            shared[threadIdx.x] = 0;
        }
        __syncthreads();

        #pragma unroll
        for (size_t j = 0; j < BLOCK_LEN; ++j) {
            tmp += mat[tid + (j + BLOCK_LEN * i) * height] * shared[j];
        }
        __syncthreads();
    }

    if (tid < height) {
        retvec[tid] = tmp;
    }
}

GpuVec mul_mat_vec_gpu(const GpuMat &mat, const GpuVec& vec) {
    assert(mat.width() == vec.len());
    GpuVec ret(N);

    simple_kernel<<<N_BLOCKS, BLOCK_LEN>>>(mat.vals, vec.vals, ret.vals, mat.height(), mat.width());
    gpuErrchk(
        cudaPeekAtLastError()
    );
    return ret;
}

GpuVec mul_mat_vec_gpu_transposed(const GpuMat &mat, const GpuVec& vec) {
    assert(mat.height() == vec.len());
    GpuVec ret(N);

    transposed_kernel<<<N_BLOCKS, BLOCK_LEN>>>(mat.vals, vec.vals, ret.vals, mat.width(), mat.height());
    gpuErrchk(
        cudaPeekAtLastError()
    );
    return ret;
}

GpuVec mul_mat_vec_gpu_shared(const GpuMat &mat, const GpuVec& vec) {
    assert(mat.width() == vec.len());
    GpuVec ret(N);

    shared_kernel<<<N_BLOCKS, BLOCK_LEN>>>(mat.vals, vec.vals, ret.vals, mat.height(), mat.width());
    gpuErrchk(
        cudaPeekAtLastError()
    );
    return ret;
}

GpuVec mul_mat_vec_gpu_shared_transposed(const GpuMat &mat, const GpuVec& vec) {
    assert(mat.height() == vec.len());
    GpuVec ret(N);

    shared_transposed_kernel<<<N_BLOCKS, BLOCK_LEN>>>(mat.vals, vec.vals, ret.vals, mat.width(), mat.height());
    gpuErrchk(
        cudaPeekAtLastError()
    );
    return ret;
}

CpuVec mul_mat_vec_cpu(const CpuMat& mat, const CpuVec& vec) {
    assert(mat.width() == vec.len());
    CpuVec ret(mat.height());
    for(size_t i = 0; i < ret.len(); ++i) {
        ret[i] = 0;
        for(size_t k = 0; k < mat.width(); ++k) {
            ret[i] += mat[i][k] * vec[k];
        }
    }
    return ret;
}

void test_naive() {
    CpuMat mat(N, M);
    CpuVec vec(M);
    mat.randomize(); vec.randomize();
    const auto mul_cpu = mul_mat_vec_cpu(mat, vec);

    const auto gpu_mat = mat.clone_to_gpu();
    const auto gpu_vec = vec.clone_to_gpu();

    const auto gpumul = mul_mat_vec_gpu(gpu_mat, gpu_vec);

    const auto gpumul_cpu = gpumul.clone_to_cpu();

    assert(CpuVec::cmp(gpumul_cpu, mul_cpu));
}

void test_transposed() {
    CpuMat mat(N, M);
    CpuVec vec(M);
    mat.randomize(); vec.randomize();

    const auto mul_cpu = mul_mat_vec_cpu(mat, vec);

    mat.transpose();

    const auto gpu_mat = mat.clone_to_gpu();
    const auto gpu_vec = vec.clone_to_gpu();

    const auto gpumul = mul_mat_vec_gpu_transposed(gpu_mat, gpu_vec);
    const auto gpumul_cpu = gpumul.clone_to_cpu();

    assert(CpuVec::cmp(gpumul_cpu, mul_cpu));
}

void test_shared() {
    CpuMat mat(N, M);
    CpuVec vec(M);
    mat.randomize(); vec.randomize();

    const auto mul_cpu = mul_mat_vec_cpu(mat, vec);

    const auto gpu_mat = mat.clone_to_gpu();
    const auto gpu_vec = vec.clone_to_gpu();

    const auto gpumul = mul_mat_vec_gpu_shared(gpu_mat, gpu_vec);
    const auto gpumul_cpu = gpumul.clone_to_cpu();

    assert(CpuVec::cmp(gpumul_cpu, mul_cpu));
}

void test_shared_transposed() {
    CpuMat mat(N, M);
    CpuVec vec(M);
    mat.randomize(); vec.randomize();

    const auto mul_cpu = mul_mat_vec_cpu(mat, vec);

    mat.transpose();

    const auto gpu_mat = mat.clone_to_gpu();
    const auto gpu_vec = vec.clone_to_gpu();

    const auto gpumul = mul_mat_vec_gpu_shared_transposed(gpu_mat, gpu_vec);
    const auto gpumul_cpu = gpumul.clone_to_cpu();

    assert(CpuVec::cmp(gpumul_cpu, mul_cpu));
}


int main() {
    test_naive();
    test_transposed();
    test_shared();
    test_shared_transposed();
    return 0;
}
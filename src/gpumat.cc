#include "gpumat.h"
#include "cpumat.h"
#include "util.h"
#include <cassert>
GpuMat::GpuMat(std::size_t dim1, std::size_t dim2)
: dim1(dim1), dim2(dim2)
{
    gpuErrchk( cudaMalloc(&vals, size()) );
}

GpuMat::GpuMat(const CpuMat& cpumat)
: GpuMat(cpumat.height(), cpumat.width())
{
    gpuErrchk( cudaMemcpy(vals, cpumat.vals, cpumat.size(), cudaMemcpyHostToDevice) );
}

GpuMat::GpuMat(std::size_t dim)
: GpuMat(dim, dim)
{}

GpuMat::GpuMat(const GpuMat& gpumat)
: GpuMat(gpumat.dim1, gpumat.dim2)
{
    gpuErrchk( cudaMemcpy(vals, gpumat.vals, gpumat.size(), cudaMemcpyDeviceToDevice) );
}

GpuMat::~GpuMat() {
    gpuErrchk( cudaFree(vals) );
}

void GpuMat::copy_to_cpu(CpuMat& cpumat) const {
    assert(dim1 == cpumat.height());
    assert(dim2 == cpumat.width());
    gpuErrchk( cudaMemcpy(cpumat.vals, vals, size(), cudaMemcpyDeviceToHost) );
}

CpuMat GpuMat::clone_to_cpu() const {
    CpuMat cpumat(dim1, dim2);
    gpuErrchk( cudaMemcpy(cpumat.vals, vals, size(), cudaMemcpyDeviceToHost) );
    return cpumat;
}

size_t GpuMat::height() const {
    return dim1;
}

size_t GpuMat::width() const {
    return dim2;
}

size_t GpuMat::len() const {
    return dim1 * dim2;
}

size_t GpuMat::size() const {
    return len() * sizeof(vals[0]);
}
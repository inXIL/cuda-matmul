#include <cassert>
#include <cstdio>
#include <iostream>
#include "cpumat.h"
#include "gpumat.h"

// gridsize is (N1 * N2) * (N1 * N2)
const size_t N1 = 32;
const size_t N2 = 32;
const size_t N = N1 * N2;

__global__ void __matmul_square(
   int* m1, int* m2, int* m, size_t sz
) {
   size_t i = blockIdx.y * blockDim.y + threadIdx.y;
   size_t j = blockIdx.x * blockDim.x + threadIdx.x;
   int tmp = 0;
   for(size_t k = 0; k < sz; ++k) {
      tmp += m1[k + i * sz] * m2[j + k * sz];
   }
   m[j + i * sz] = tmp;
}

// if i transpose the other matrix it goes 10 times slower, like this it's a little slower
__global__ void __matmul_square_transposed(
   int* m1, int* m2, int* m, size_t sz
) {
   size_t i = blockIdx.y * blockDim.y + threadIdx.y;
   size_t j = blockIdx.x * blockDim.x + threadIdx.x;
   int tmp = 0;
   for(size_t k = 0; k < sz; ++k) {
      tmp += m1[i + k * sz] * m2[j + k * sz];
   }
   m[j + i * sz] = tmp;
}


// didn't bother with allocating shared mem dynamically
__global__ void __matmul_square_tiled_shared_mem(
   int* m1, int* m2, int* m
) {
   __shared__ int s1[N], s2[N];

   size_t i = blockIdx.y * blockDim.y + threadIdx.y;
   size_t j = blockIdx.x * blockDim.x + threadIdx.x;

   int tmp = 0;
   const size_t id = threadIdx.y * blockDim.x + threadIdx.x;
   for(size_t k = 0; k < N; k += blockDim.x) {
      s1[id] = m1[i * N + k + threadIdx.x];
      s2[id] = m2[k * N + threadIdx.y * N + j];
      __syncthreads();

      for (size_t l = 0; l < blockDim.x; ++l) {
         tmp += s1[threadIdx.y * blockDim.x + l] * s2[l * blockDim.x + threadIdx.x];
      }
      __syncthreads();
   }
   m[j + i * N] = tmp;
}

GpuMat matmul_gpu_square(const GpuMat& m1, const GpuMat& m2, size_t n_threads_per_block) {
   assert(m1.dim1 == m1.dim2 && m1.dim2 == m2.dim1 && m2.dim1 == m2.dim2);
   const size_t dim = m1.dim1;
   GpuMat mat(dim);
   size_t n_blocks = (dim + n_threads_per_block - 1) / n_threads_per_block;

   dim3 threads(n_threads_per_block, n_threads_per_block);
   dim3 blocks(n_blocks, n_blocks);

   __matmul_square<<<blocks, threads>>>(m1.vals, m2.vals, mat.vals, dim);
   return mat;
}

GpuMat matmul_gpu_square_transposed(const GpuMat& m1, const GpuMat& m2, size_t n_threads_per_block) {
   assert(m1.dim1 == m1.dim2 && m1.dim2 == m2.dim1 && m2.dim1 == m2.dim2);
   const size_t dim = m1.dim1;
   GpuMat mat(dim);
   size_t n_blocks = (dim + n_threads_per_block - 1) / n_threads_per_block;

   dim3 threads(n_threads_per_block, n_threads_per_block);
   dim3 blocks(n_blocks, n_blocks);

   __matmul_square_transposed<<<blocks, threads>>>(m1.vals, m2.vals, mat.vals, dim);
   return mat;
}

GpuMat matmul_gpu_square_tiled_shared_mem(const GpuMat& m1, const GpuMat& m2, size_t n_threads_per_block) {
   assert(m1.dim1 == m1.dim2 && m1.dim2 == m2.dim1 && m2.dim1 == m2.dim2);
   const size_t dim = m1.dim1;
   GpuMat mat(dim);
   size_t n_blocks = (dim + n_threads_per_block - 1) / n_threads_per_block;

   dim3 threads(n_threads_per_block, n_threads_per_block);
   dim3 blocks(n_blocks, n_blocks);

   __matmul_square_tiled_shared_mem<<<blocks, threads>>>(m1.vals, m2.vals, mat.vals);
   return mat;
}


void square(size_t thread_multiplier, size_t n_threads_per_block) {
   const size_t n = thread_multiplier * n_threads_per_block;
   CpuMat cpumat1(n), cpumat2(n);
   cpumat1.randomize(); cpumat2.randomize();
   GpuMat gpumat1 = cpumat1.gpu_clone(), gpumat2 = cpumat2.gpu_clone();
   const auto cpumul = matmul_cpu(cpumat1, cpumat2);
   const auto gpumul = matmul_gpu_square(gpumat1, gpumat2, n_threads_per_block);
   const auto gpumul_cpu = gpumul.clone_to_cpu();
   assert(matcmp_cpu(cpumul, gpumul_cpu));
}
void square_transposed(size_t thread_multiplier, size_t n_threads_per_block) {
   const size_t n = thread_multiplier * n_threads_per_block;
   CpuMat cpumat1(n), cpumat2(n);
   cpumat1.randomize(); cpumat2.randomize();
   const auto cpumul = matmul_cpu(cpumat1, cpumat2);
   cpumat1.transpose();
   GpuMat gpumat1 = cpumat1.gpu_clone(), gpumat2 = cpumat2.gpu_clone();
   const auto gpumul = matmul_gpu_square_transposed(gpumat1, gpumat2, n_threads_per_block);
   const auto gpumul_cpu = gpumul.clone_to_cpu();
   assert(matcmp_cpu(cpumul, gpumul_cpu));
}
void square_tiled_shared_mem(size_t thread_multiplier, size_t n_threads_per_block) {
   const size_t n = thread_multiplier * n_threads_per_block;
   CpuMat cpumat1(n), cpumat2(n);
   cpumat1.randomize(); cpumat2.randomize();
   GpuMat gpumat1 = cpumat1.gpu_clone(), gpumat2 = cpumat2.gpu_clone();
   const auto cpumul = matmul_cpu(cpumat1, cpumat2);
   const auto gpumul = matmul_gpu_square_tiled_shared_mem(gpumat1, gpumat2, n_threads_per_block);
   const auto gpumul_cpu = gpumul.clone_to_cpu();
   assert(matcmp_cpu(cpumul, gpumul_cpu));
}

int main() {
   square(N1, N2);
   square_transposed(N1, N2);
   square_tiled_shared_mem(N1, N2);
   return 0;
}
#ifndef CPUMAT_H
#define CPUMAT_H
#include <cstddef>
struct GpuMat;
struct CpuMat {
    CpuMat(std::size_t dim1, std::size_t dim2);
    CpuMat(std::size_t dim);
    CpuMat(const CpuMat& mat);
    ~CpuMat();
    int* operator[](size_t id);
    const int* operator[](size_t id) const;
    GpuMat clone_to_gpu() const;
    void randomize();
    void println() const;
    void transpose();
    static CpuMat mul(const CpuMat& m1, const CpuMat& m2);
    static bool cmp(const CpuMat& m1, const CpuMat& m2);
    size_t height() const;
    size_t width() const;
    size_t len() const;
    size_t size() const;
public:
    int* vals;
private:
    std::size_t dim1, dim2;
};
#endif
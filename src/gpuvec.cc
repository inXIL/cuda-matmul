#include "gpuvec.h"
#include "cpuvec.h"
#include "util.h"
#include <memory.h>
#include <cassert>
GpuVec::GpuVec(size_t n)
: n_elems(n)
{
    gpuErrchk(cudaMalloc(&vals, n * sizeof(int)));
}

GpuVec::~GpuVec() {
    gpuErrchk(cudaFree(vals));
}

GpuVec::GpuVec(const GpuVec& vec)
: GpuVec(vec.len())
{
    memcpy(vals, vec.vals, vec.len() * sizeof(vals[0]));
}

GpuVec::GpuVec(const CpuVec& vec)
: GpuVec(vec.len())
{
    gpuErrchk( cudaMemcpy(vals, vec.vals, vec.len() * sizeof(vec[0]), cudaMemcpyHostToDevice) );
}

size_t GpuVec::len() const {
    return n_elems;
}

CpuVec GpuVec::clone_to_cpu() const {
    return CpuVec(*this);
}

// GpuVec GpuVec::shrink_clone(size_t n_elem) const {
//     assert(n_elem < dim1);
//     GpuVec ret(n_elem);
//     gpuErrchk( cudaMemcpy(ret.vals, vals, ret.size, cudaMemcpyDeviceToDevice) );
//     return ret;
// }
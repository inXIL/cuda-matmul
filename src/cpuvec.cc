#include "cpuvec.h"
#include "gpuvec.h"
#include "util.h"
#include <memory.h>
#include <cassert>

CpuVec::CpuVec(size_t n)
: n_elems(n), vals(new int[n])
{}

CpuVec::~CpuVec() {
    delete[] vals;
}

CpuVec::CpuVec(const CpuVec& vec)
: CpuVec(vec.len())
{
    memcpy(vals, vec.vals, vec.len() * sizeof(vec[0]));
}

CpuVec::CpuVec(const GpuVec& vec)
: CpuVec(vec.len())
{
    gpuErrchk(cudaMemcpy(vals, vec.vals, vec.len() * sizeof(vals[0]), cudaMemcpyDeviceToHost));
}

size_t CpuVec::len() const { return n_elems; }

int& CpuVec::operator[](size_t id) { return vals[id]; }

const int& CpuVec::operator[](size_t id) const { return vals[id]; }

void CpuVec::randomize() {
    cpu::randomize_array(vals, n_elems);
}

GpuVec CpuVec::clone_to_gpu() const {
    return GpuVec(*this);
}

bool CpuVec::cmp(const CpuVec& vec1, const CpuVec& vec2) {
    if (vec1.len() != vec2.len()) return false;
    for(size_t i = 0; i < vec1.len(); ++i) {
        if (vec1[i] != vec2[i]) return false;
    }
    return true;
}